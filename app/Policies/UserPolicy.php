<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function index(User $auth)
    {
        return true;
        return $auth->isAdmin();
    }

    public function create(User $auth)
    {
        return true;
        return $auth->isAdmin();
    }

    public function edit(User $auth, User $user)
    {
        return true;
        return  $auth->id == $user->id  || $auth->isAdmin();
    }

    public function destroy(User $auth)
    {
        return true;
        return  $auth->isAdmin();
    }
}
